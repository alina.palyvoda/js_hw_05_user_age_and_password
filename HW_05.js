/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
//1.Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
    // Экранирование - это замена так называемых управляющих символов на обычные символы, то есть
    // для того, чтобы эти символы распознавались как обычный текст, а не часть кода. Так же экранирование
    // помогает вносить в текст символы, которых нет на клавиатуре и осуществлять поиск необходимых символов.

const createNewUser = () => {
        let firstName = prompt("Enter your name");
        let lastName = prompt("Enter your last name");
        let userBirthday = prompt("Enter your date of birth", "01.01.1970");
        let newDate = new Date();
        let userYear = userBirthday.slice(6);
        let userMonth = userBirthday.slice(3, 5) - 1;
        let userDay = userBirthday.slice(0, 3);
        let dayOfBirth = new Date(userYear, userMonth, userDay);
        const newUser = {
            firstName,
            lastName,
            dayOfBirth,
            getLogin: function () {
                return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
            },
            getAge: function () {
                let dayOfBirthNow = new Date(newDate.getFullYear(), userMonth, userDay);
                let userAge = newDate.getFullYear() - dayOfBirth.getFullYear();
                if (dayOfBirthNow > dayOfBirth) {
                    userAge = userAge - 1;
                }
                return userAge;
            },
            getPassword: function () {
                return this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + userYear;
            },
        }
        return newUser;
    }

let user = createNewUser();
console.log(user)
console.log(`Your password: ${user.getPassword()}`);
console.log(`Your age: ${user.getAge()} years`);

